type WordSearch = Vec<Vec<char>>;

fn parse(input: &str) -> WordSearch {
    input
        .lines()
        .collect::<Vec<_>>()
        .iter()
        .map(|s| s.chars().collect())
        .collect()
}

fn search_north(i: usize, j: usize, word_search: &WordSearch) -> bool {
    i > 2
        && word_search[i][j] == 'X'
        && word_search[i - 1][j] == 'M'
        && word_search[i - 2][j] == 'A'
        && word_search[i - 3][j] == 'S'
}

fn search_south(i: usize, j: usize, word_search: &WordSearch) -> bool {
    i < word_search.len() - 3
        && word_search[i][j] == 'X'
        && word_search[i + 1][j] == 'M'
        && word_search[i + 2][j] == 'A'
        && word_search[i + 3][j] == 'S'
}

fn search_east(i: usize, j: usize, word_search: &WordSearch) -> bool {
    j < word_search[i].len() - 3
        && word_search[i][j] == 'X'
        && word_search[i][j + 1] == 'M'
        && word_search[i][j + 2] == 'A'
        && word_search[i][j + 3] == 'S'
}

fn search_west(i: usize, j: usize, word_search: &WordSearch) -> bool {
    j > 2
        && word_search[i][j] == 'X'
        && word_search[i][j - 1] == 'M'
        && word_search[i][j - 2] == 'A'
        && word_search[i][j - 3] == 'S'
}

fn search_ne(i: usize, j: usize, word_search: &WordSearch) -> bool {
    i > 2
        && j < word_search[i].len() - 3
        && word_search[i][j] == 'X'
        && word_search[i - 1][j + 1] == 'M'
        && word_search[i - 2][j + 2] == 'A'
        && word_search[i - 3][j + 3] == 'S'
}

fn search_nw(i: usize, j: usize, word_search: &WordSearch) -> bool {
    i > 2
        && j > 2
        && word_search[i][j] == 'X'
        && word_search[i - 1][j - 1] == 'M'
        && word_search[i - 2][j - 2] == 'A'
        && word_search[i - 3][j - 3] == 'S'
}

fn search_se(i: usize, j: usize, word_search: &WordSearch) -> bool {
    i < word_search.len() - 3
        && j < word_search[i].len() - 3
        && word_search[i][j] == 'X'
        && word_search[i + 1][j + 1] == 'M'
        && word_search[i + 2][j + 2] == 'A'
        && word_search[i + 3][j + 3] == 'S'
}

fn search_sw(i: usize, j: usize, word_search: &WordSearch) -> bool {
    i < word_search.len() - 3
        && j > 2
        && word_search[i][j] == 'X'
        && word_search[i + 1][j - 1] == 'M'
        && word_search[i + 2][j - 2] == 'A'
        && word_search[i + 3][j - 3] == 'S'
}

fn search_in(word_search: &WordSearch) -> usize {
    let mut count = 0;
    for i in 0..word_search.len() {
        for j in 0..word_search[i].len() {
            if word_search[i][j] == 'X' {
                if search_north(i, j, word_search) {
                    count += 1;
                }
                if search_south(i, j, word_search) {
                    count += 1;
                }
                if search_west(i, j, word_search) {
                    count += 1;
                }
                if search_east(i, j, word_search) {
                    count += 1;
                }
                if search_ne(i, j, word_search) {
                    count += 1;
                }
                if search_nw(i, j, word_search) {
                    count += 1;
                }
                if search_se(i, j, word_search) {
                    count += 1;
                }
                if search_sw(i, j, word_search) {
                    count += 1;
                }
            }
        }
    }
    count
}

pub fn compute(input: &str) -> usize {
    let word_search = parse(input);
    search_in(&word_search)
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX
";
    const EXPECTED: usize = 18;

    #[test]
    fn compute_d4p1() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED);
    }
}
