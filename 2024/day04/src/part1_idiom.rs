type WordSearch = Vec<Vec<char>>;

fn parse(input: &str) -> WordSearch {
    input
        .lines()
        .collect::<Vec<_>>()
        .iter()
        .map(|s| s.chars().collect())
        .collect()
}

enum Direction {
    North,
    South,
    East,
    West,
    NE,
    NW,
    SE,
    SW,
}

fn is_found(
    word: &str,
    direction: Direction,
    i: usize,
    j: usize,
    word_search: &WordSearch,
) -> bool {
    let word = word.chars().collect::<Vec<_>>();
    let word_max_idx = word.len() - 1;
    match direction {
        Direction::North if i >= word_max_idx => {
            let mut cmp_word = vec![];
            for k in 0..word.len() {
                cmp_word.push(word_search[i - k][j]);
            }
            word == cmp_word
        }
        Direction::South if i < word_search.len() - word_max_idx => {
            let mut cmp_word = vec![];
            for k in 0..word.len() {
                cmp_word.push(word_search[i + k][j]);
            }
            word == cmp_word
        }
        Direction::East if j < word_search[i].len() - word_max_idx => {
            let mut cmp_word = vec![];
            for k in 0..word.len() {
                cmp_word.push(word_search[i][j + k]);
            }
            word == cmp_word
        }
        Direction::West if j >= word_max_idx => {
            let mut cmp_word = vec![];
            for k in 0..word.len() {
                cmp_word.push(word_search[i][j - k]);
            }
            word == cmp_word
        }
        Direction::NE if i >= word_max_idx && j < word_search[i].len() - word_max_idx => {
            let mut cmp_word = vec![];
            for k in 0..word.len() {
                cmp_word.push(word_search[i - k][j + k]);
            }
            word == cmp_word
        }
        Direction::NW if i >= word_max_idx && j >= word_max_idx => {
            let mut cmp_word = vec![];
            for k in 0..word.len() {
                cmp_word.push(word_search[i - k][j - k]);
            }
            word == cmp_word
        }
        Direction::SE
            if i < word_search.len() - word_max_idx && j < word_search[i].len() - word_max_idx =>
        {
            let mut cmp_word = vec![];
            for k in 0..word.len() {
                cmp_word.push(word_search[i + k][j + k]);
            }
            word == cmp_word
        }
        Direction::SW if i < word_search.len() - word_max_idx && j >= word_max_idx => {
            let mut cmp_word = vec![];
            for k in 0..word.len() {
                cmp_word.push(word_search[i + k][j - k]);
            }
            word == cmp_word
        }
        _ => false,
    }
}

fn search(word_search: &WordSearch, word: &str) -> usize {
    let mut count = 0;
    for i in 0..word_search.len() {
        for j in 0..word_search[i].len() {
            if word_search[i][j] == word.chars().next().unwrap() {
                if is_found(word, Direction::North, i, j, word_search) {
                    count += 1;
                }
                if is_found(word, Direction::South, i, j, word_search) {
                    count += 1;
                }
                if is_found(word, Direction::East, i, j, word_search) {
                    count += 1;
                }
                if is_found(word, Direction::West, i, j, word_search) {
                    count += 1;
                }
                if is_found(word, Direction::NE, i, j, word_search) {
                    count += 1;
                }
                if is_found(word, Direction::NW, i, j, word_search) {
                    count += 1;
                }
                if is_found(word, Direction::SE, i, j, word_search) {
                    count += 1;
                }
                if is_found(word, Direction::SW, i, j, word_search) {
                    count += 1;
                }
            }
        }
    }
    count
}

pub fn compute(input: &str) -> usize {
    let word_search = parse(input);
    const WORD: &str = "XMAS";
    search(&word_search, WORD)
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX
";
    const EXPECTED: usize = 18;

    #[test]
    fn compute_d4p1() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED);
    }
}
