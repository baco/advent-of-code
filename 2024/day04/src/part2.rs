type WordSearch = Vec<Vec<char>>;

fn parse(input: &str) -> WordSearch {
    input
        .lines()
        .collect::<Vec<_>>()
        .iter()
        .map(|s| s.chars().collect())
        .collect()
}

fn search_in(word_search: &WordSearch) -> usize {
    let mut count = 0;
    for i in 1..word_search.len() - 1 {
        for j in 1..word_search[i].len() - 1 {
            if word_search[i][j] == 'A'
                && ((word_search[i - 1][j - 1] == 'M'
                    && word_search[i + 1][j + 1] == 'S'
                    && word_search[i + 1][j - 1] == 'M'
                    && word_search[i - 1][j + 1] == 'S')
                    || (word_search[i - 1][j - 1] == 'M'
                        && word_search[i + 1][j + 1] == 'S'
                        && word_search[i - 1][j + 1] == 'M'
                        && word_search[i + 1][j - 1] == 'S')
                    || (word_search[i - 1][j + 1] == 'M'
                        && word_search[i + 1][j - 1] == 'S'
                        && word_search[i + 1][j + 1] == 'M'
                        && word_search[i - 1][j - 1] == 'S')
                    || (word_search[i + 1][j + 1] == 'M'
                        && word_search[i - 1][j - 1] == 'S'
                        && word_search[i + 1][j - 1] == 'M'
                        && word_search[i - 1][j + 1] == 'S'))
            {
                count += 1;
            }
        }
    }
    count
}

pub fn compute(input: &str) -> usize {
    let word_search = parse(input);
    search_in(&word_search)
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX
";
    const EXPECTED: usize = 9;

    #[test]
    fn compute_d4p2() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED);
    }
}
