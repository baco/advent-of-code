fn parse_input(input: &str) -> Vec<Vec<u32>> {
    let mut result: Vec<Vec<u32>> = vec![];
    for line_str in input.lines() {
        let line = line_str.split_whitespace().map(|s| s.parse().unwrap());
        result.push(line.collect());
    }
    result
}

fn is_valid(record: &[u32]) -> bool {
    let dec = record[0] > record[1];
    let inc = record[0] < record[1];
    for i in 1..record.len() {
        if (dec
            && (record[i] >= record[i - 1]
                || record[i - 1].abs_diff(record[i]) > 3
                || record[i - 1].abs_diff(record[i]) < 1))
            || (inc
                && (record[i - 1] >= record[i]
                    || record[i].abs_diff(record[i - 1]) > 3
                    || record[i].abs_diff(record[i - 1]) < 1))
        {
            return false;
        }
    }
    dbg!(&record, &dec, &inc);
    dec || inc
}

pub fn compute(input: &str) -> String {
    let records = parse_input(input);
    records
        .iter()
        .filter(|record| {
            is_valid(record)
                || (0..record.len()).any(|i| is_valid(&[&record[..i], &record[i + 1..]].concat()))
        })
        .count()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9
";
    const EXPECTED: usize = 4;

    #[test]
    fn compute_d2p1() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
