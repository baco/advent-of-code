use day02::part1_idiom::compute;

fn main() {
    let file = include_str!("../../input.txt");
    println!("{}", compute(file));
}
