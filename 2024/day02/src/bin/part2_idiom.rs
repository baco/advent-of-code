use day02::part2_idiom::compute;

fn main() {
    let file = include_str!("../../input.txt");
    println!("{}", compute(file));
}
