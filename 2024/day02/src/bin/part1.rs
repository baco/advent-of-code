use day02::part1::compute;

fn main() {
    let file = include_str!("../../input.txt");
    println!("{}", compute(file));
}
