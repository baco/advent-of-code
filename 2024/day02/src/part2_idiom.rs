type Report = Vec<u32>;

fn parse_input(input: &str) -> Vec<Report> {
    let mut result: Vec<Report> = vec![];
    for line_str in input.lines() {
        let line = line_str.split_whitespace().map(|s| s.parse().unwrap());
        result.push(line.collect());
    }
    result
}

fn is_valid(record: &Report) -> bool {
    record.windows(2).all(|w| w[0] < w[1] && w[1] - w[0] <= 3)
        || record.windows(2).all(|w| w[0] > w[1] && w[0] - w[1] <= 3)
}

pub fn compute(input: &str) -> usize {
    let records = parse_input(input);
    records
        .iter()
        .filter(|record| {
            is_valid(record)
                || (0..record.len()).any(|i| is_valid(&[&record[..i], &record[i + 1..]].concat()))
        })
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9
";
    const EXPECTED: usize = 4;

    #[test]
    fn compute_d2p1() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED);
    }
}
