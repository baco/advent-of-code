Diff between parts 1 and 2 is left for educational purpose.

Most of the stuff in part2 can be copied back to part1, as it's basically the
same problem with minor changes, and part2 started from part1 but evolved.

The idea is to have both versions present, so with the diff between files we
see a "normal" version of the code in part1 and a "refactored"/"Rust-idiomatic"
version of the code in part2.
