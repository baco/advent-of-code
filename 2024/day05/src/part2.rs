type Rule = Vec<u32>;
type Update = Vec<u32>;

fn load<T: FromIterator<u32>>(input: &str, sep: char) -> Vec<T> {
    input
        .lines()
        .map(|l| l.split(sep).map(|n| n.parse().unwrap()).collect::<T>())
        .collect()
}

fn parse(input: &str) -> (String, String) {
    let mut rules_str = vec![];
    let mut updates_str = vec![];

    let mut rules_stop = false;
    for l in input.lines() {
        if l.is_empty() {
            rules_stop = true;
            continue;
        }
        if !rules_stop {
            rules_str.push(l);
        } else {
            updates_str.push(l);
        }
    }
    (rules_str.join("\n"), updates_str.join("\n"))
}

fn is_valid(update: &Update, rules: &[Rule]) -> bool {
    rules.iter().fold(true, |acc, rule| {
        let (Some(r1_pos), Some(r2_pos)) = (
            update.iter().position(|&x| x == rule[0]),
            update.iter().position(|&x| x == rule[1]),
        ) else {
            return acc;
        };
        acc && r1_pos < r2_pos
    })
}

fn fix(update: &Update, rules: &[Rule]) -> Update {
    let mut update = update.clone();
    while !is_valid(&update, rules) {
        for rule in rules {
            let (Some(r1_pos), Some(r2_pos)) = (
                update.iter().position(|&x| x == rule[0]),
                update.iter().position(|&x| x == rule[1]),
            ) else {
                continue;
            };
            if r1_pos < r2_pos {
                continue;
            }
            (update[r1_pos], update[r2_pos]) = (rule[1], rule[0]);
        }
    }
    update
}

fn get_middle_pages(update: &Update) -> u32 {
    update[update.len() / 2]
}

pub fn compute(input: &str) -> u32 {
    let (rules_str, updates_str) = parse(input);
    dbg!(&rules_str, &updates_str);

    let rules: Vec<Rule> = load(&rules_str, '|');
    let updates: Vec<Update> = load(&updates_str, ',');
    dbg!(&rules, &updates);
    updates
        .iter()
        .filter(|&u| {
            dbg!(u, !is_valid(u, &rules));
            !is_valid(u, &rules)
        })
        .map(|u| {
            let fixed = fix(u, &rules);
            get_middle_pages(&fixed)
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47
";
    const EXPECTED: u32 = 123;

    #[test]
    fn compute_d5p2() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED);
    }
}
