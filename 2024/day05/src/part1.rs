type Rule = Vec<u32>;

fn load_rules(input: &str) -> Vec<Rule> {
    input
        .lines()
        .map(|l| l.split('|').map(|n| n.parse().unwrap()).collect())
        .collect()
}

type Update = Vec<u32>;

fn load_updates(input: &str) -> Vec<Update> {
    input
        .lines()
        .map(|l| l.split(',').map(|n| n.parse().unwrap()).collect())
        .collect()
}

fn parse(input: &str) -> (String, String) {
    let mut rules_str = vec![];
    let mut updates_str = vec![];

    let mut rules_stop = false;
    for l in input.lines() {
        if l.is_empty() {
            rules_stop = true;
            continue;
        }
        if !rules_stop {
            rules_str.push(l);
        } else {
            updates_str.push(l);
        }
    }
    (rules_str.join("\n"), updates_str.join("\n"))
}

fn is_valid(update: &Update, rules: &[Rule]) -> bool {
    rules.iter().fold(true, |acc, rule| {
        if !update.contains(&rule[0]) || !update.contains(&rule[1]) {
            return acc;
        }
        let r1_pos = update.iter().position(|&x| x == rule[0]).unwrap();
        let r2_pos = update.iter().position(|&x| x == rule[1]).unwrap();
        acc && r1_pos < r2_pos
    })
}

fn get_middle_pages(update: &Update) -> u32 {
    update[update.len() / 2]
}

pub fn compute(input: &str) -> u32 {
    let (rules_str, updates_str) = parse(input);
    dbg!(&rules_str, &updates_str);

    let rules = load_rules(&rules_str);
    let updates = load_updates(&updates_str);
    dbg!(&rules, &updates);
    updates
        .iter()
        .filter(|&u| {
            dbg!(u, is_valid(u, &rules));
            is_valid(u, &rules)
        })
        .map(get_middle_pages)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47
";
    const EXPECTED: u32 = 143;

    #[test]
    fn compute_d5p1() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED);
    }
}
