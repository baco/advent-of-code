#[derive(Debug)]
struct Equation {
    lhs: u64,
    rhs: Vec<u64>,
}

fn parse(input: &str) -> Vec<Equation> {
    input
        .lines()
        .map(|l| {
            let eq_parts: Vec<_> = l.split(": ").collect();
            Equation {
                lhs: eq_parts[0].parse().unwrap(),
                rhs: eq_parts[1]
                    .split_whitespace()
                    .map(|s| s.parse().unwrap())
                    .collect(),
            }
        })
        .collect()
}

fn is_valid(equation: &Equation) -> bool {
    let mut values_iter = equation.rhs.iter();
    let mut accs = vec![values_iter.next().unwrap().to_owned()];
    for value in values_iter {
        let mut next_accs = vec![];
        for acc in accs.iter() {
            next_accs.push(acc + value);
            next_accs.push(acc * value);
        }
        accs = next_accs;
    }
    accs.contains(&equation.lhs)
}

pub fn compute(input: &str) -> u64 {
    let equations = parse(input);
    equations
        .iter()
        .filter(|&e| is_valid(e))
        .map(|e| e.lhs)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
190: 10 19
3267: 81 40 27
83: 17 5
156: 15 6
7290: 6 8 6 15
161011: 16 10 13
192: 17 8 14
21037: 9 7 18 13
292: 11 6 16 20
";
    const EXPECTED: u64 = 3749;

    #[test]
    fn compute_d7p1() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED);
    }
}
