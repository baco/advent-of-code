fn read_input(input: &str) -> (Vec<u32>, Vec<u32>) {
    let mut left: Vec<u32> = Vec::new();
    let mut right: Vec<u32> = Vec::new();

    for line_str in input.lines() {
        let mut line = line_str.split_whitespace().map(|s| s.parse().unwrap());
        left.push(line.next().unwrap());
        right.push(line.next().unwrap());
    }

    (left, right)
}

fn sum_difs(vec1: &[u32], vec2: &[u32]) -> u32 {
    let mut sum = 0u32;
    let it = vec1.iter().zip(vec2.iter());
    for (a, b) in it {
        sum += if a > b { a - b } else { b - a };
    }
    sum
}

pub fn compute(input: &str) -> String {
    let (mut left, mut right) = read_input(input);
    left.sort();
    right.sort();
    let sum = sum_difs(&left, &right);
    sum.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
3   4
4   3
2   5
1   3
3   9
3   3
";
    const EXPECTED: u32 = 11;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
