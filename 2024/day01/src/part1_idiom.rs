fn read_input(input: &str) -> (Vec<u32>, Vec<u32>) {
    let mut left: Vec<u32> = vec![];
    let mut right: Vec<u32> = vec![];

    for line_str in input.lines() {
        let mut line = line_str.split_whitespace().map(|s| s.parse().unwrap());
        left.push(line.next().unwrap());
        right.push(line.next().unwrap());
    }

    (left, right)
}

fn sum_difs(vec1: &[u32], vec2: &[u32]) -> u32 {
    std::iter::zip(vec1, vec2)
        .map(|(&a, &b)| a.abs_diff(b))
        .sum()
}

pub fn compute(input: &str) -> u32 {
    let (mut left, mut right) = read_input(input);
    left.sort();
    right.sort();
    sum_difs(&left, &right)
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
3   4
4   3
2   5
1   3
3   9
3   3
";
    const EXPECTED: u32 = 11;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED);
    }
}
