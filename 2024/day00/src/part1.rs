pub fn compute(_input: &str) -> String {
    todo!("day 00, part 1");
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
";
    const EXPECTED: usize = 13;

    #[test]
    fn compute_d0p1() {
        todo!("no test yet");
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
