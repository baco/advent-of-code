#[derive(Debug, Clone, PartialEq)]
enum Tile {
    Empty,
    Obstacle,
    Guard(Direction),
    Breadcrumb,
}

#[derive(Debug, Clone, PartialEq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

fn parse(input: &str) -> Vec<Vec<Tile>> {
    input
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| match c {
                    '.' => Tile::Empty,
                    '#' => Tile::Obstacle,
                    '^' => Tile::Guard(Direction::Up),
                    'v' => Tile::Guard(Direction::Down),
                    '>' => Tile::Guard(Direction::Right),
                    '<' => Tile::Guard(Direction::Left),
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect()
}

fn locate_guard(map: &[Vec<Tile>]) -> (Tile, (usize, usize)) {
    for (i, row) in map.iter().enumerate() {
        for (j, tile) in row.iter().enumerate() {
            if let Tile::Guard(_) = tile {
                return (tile.clone(), (i, j));
            }
        }
    }
    unreachable!()
}

fn turn(guard: &mut Tile, map: &mut [Vec<Tile>], pos: &(usize, usize)) {
    match guard {
        Tile::Guard(Direction::Up) => {
            *guard = Tile::Guard(Direction::Right);
            map[pos.0][pos.1] = guard.clone();
        }
        Tile::Guard(Direction::Down) => {
            *guard = Tile::Guard(Direction::Left);
            map[pos.0][pos.1] = guard.clone();
        }
        Tile::Guard(Direction::Left) => {
            *guard = Tile::Guard(Direction::Up);
            map[pos.0][pos.1] = guard.clone();
        }
        Tile::Guard(Direction::Right) => {
            *guard = Tile::Guard(Direction::Down);
            map[pos.0][pos.1] = guard.clone();
        }
        _ => unreachable!(),
    }
}

fn walk(map: &mut [Vec<Tile>], start: &(usize, usize), guard: &mut Tile) -> usize {
    let mut pos = *start;
    let mut visited = 1;

    while pos.0 != 0 && pos.1 != 0 && pos.0 != map.len() - 1 && pos.1 != map[0].len() - 1 {
        map[pos.0][pos.1] = Tile::Breadcrumb;
        pos = match guard {
            Tile::Guard(Direction::Up) if map[pos.0 - 1][pos.1] != Tile::Obstacle => {
                (pos.0 - 1, pos.1)
            }
            Tile::Guard(Direction::Down) if map[pos.0 + 1][pos.1] != Tile::Obstacle => {
                (pos.0 + 1, pos.1)
            }
            Tile::Guard(Direction::Left) if map[pos.0][pos.1 - 1] != Tile::Obstacle => {
                (pos.0, pos.1 - 1)
            }
            Tile::Guard(Direction::Right) if map[pos.0][pos.1 + 1] != Tile::Obstacle => {
                (pos.0, pos.1 + 1)
            }
            _ => {
                turn(guard, map, &pos);
                continue;
            }
        };
        if map[pos.0][pos.1] == Tile::Empty {
            visited += 1;
        }
        map[pos.0][pos.1] = guard.clone();
    }

    visited
}

pub fn compute(input: &str) -> usize {
    let mut map = parse(input);
    let mut guard_pos = locate_guard(&map);

    walk(&mut map, &guard_pos.1, &mut guard_pos.0)
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#...
";
    const EXPECTED: usize = 41;

    #[test]
    fn compute_d6p1() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED);
    }
}
