use regex::Regex;

fn sum_mul(input: &str) -> u32 {
    let re = Regex::new(r"mul\((\d+),(\d+)\)").unwrap();
    re.captures_iter(input)
        .map(|cap| cap[1].parse::<u32>().unwrap() * cap[2].parse::<u32>().unwrap())
        .sum()
}

pub fn compute(input: &str) -> u32 {
    input.lines().map(sum_mul).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))
";
    const EXPECTED: u32 = 48;

    #[test]
    fn compute_d2p2() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED);
    }
}
