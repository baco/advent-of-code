use nom::{
    character::complete::{newline, one_of},
    multi::{many1, separated_list1},
    sequence::pair,
    IResult,
};

fn ident(input: &str) -> IResult<&str, Vec<char>> {
    many1(one_of("#."))(input)
}

fn block(input: &str) -> IResult<&str, Vec<Vec<char>>> {
    separated_list1(newline, ident)(input)
}

fn block_list(input: &str) -> IResult<&str, Vec<Vec<Vec<char>>>> {
    separated_list1(pair(newline, newline), block)(input)
}

pub fn compute(input: &str) -> String {
    let (_, valleys) = block_list(input.trim_end()).unwrap();
    dbg!(&valleys);
    valleys.len().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#
";
    const EXPECTED: usize = 405;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
