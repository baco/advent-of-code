use core::str::Lines;
use itertools::Itertools;

fn parse_input(lines: Lines<'_>) -> Vec<(usize, usize)> {
    lines
        .clone()
        .enumerate()
        .map(|(i, l)| {
            l.chars()
                .enumerate()
                .filter(|(_, c)| *c == '#')
                .map(|(j, _)| (i, j))
                .collect::<Vec<(usize, usize)>>()
        })
        .fold(Vec::new(), |mut v, mvec| {
            v.extend(mvec);
            v
        })
}

fn expand_galaxies(galaxies: Vec<(usize, usize)>) -> Vec<(usize, usize)> {
    let mut tmp_galaxies = galaxies.clone();
    // Expand horizontally
    tmp_galaxies.sort_by(|g1, g2| g1.1.cmp(&g2.1));
    for ((i, g1), (_, g2)) in tmp_galaxies.clone().iter().enumerate().tuple_windows() {
        if g2.1.saturating_sub(1) > g1.1 {
            for galaxy in tmp_galaxies.iter_mut().skip(i + 1) {
                galaxy.1 += g2.1 - 1 - g1.1;
            }
        }
    }

    // Expand vertically
    tmp_galaxies.sort_by(|g1, g2| g1.0.cmp(&g2.0));
    for ((i, g1), (_, g2)) in tmp_galaxies.clone().iter().enumerate().tuple_windows() {
        if g2.0.saturating_sub(1) > g1.0 {
            for galaxy in tmp_galaxies.iter_mut().skip(i + 1) {
                galaxy.0 += g2.0 - 1 - g1.0;
            }
        }
    }
    //tmp_galaxies = tmp_galaxies.clone()
    //    .iter()
    //    .rev()
    //    .tuple_windows()
    //    .map(|(&(mut g2), g1)| {
    //        if g1.0 < g2.0 {
    //            g2.0 += 2 * (g2.0 - g1.0 - 1);
    //            g2
    //        } else {
    //            g2
    //        }
    //    }).collect_vec();

    tmp_galaxies
}

fn shortest_paths_sum(galaxies: Vec<(usize, usize)>) -> usize {
    let distances_sum = galaxies
        .iter()
        .combinations(2)
        .map(|v| {
            let [g1, g2] = <[&(usize, usize); 2]>::try_from(v).ok().unwrap();
            (g2.0).abs_diff(g1.0) + (g2.1).abs_diff(g1.1)
        })
        .sum();
    distances_sum
}

pub fn compute(input: &str) -> String {
    let galaxies = parse_input(input.lines());
    let expanded_galaxies = expand_galaxies(galaxies);
    let shortest_paths_sum = shortest_paths_sum(expanded_galaxies);
    shortest_paths_sum.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
";
    const EXPECTED: usize = 374;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
