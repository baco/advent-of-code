use day11::part2::compute;

fn main() {
    let file = include_str!("../../input.txt");
    const MULTIPLIER: usize = 1_000_000;
    println!("{}", compute(file, MULTIPLIER));
}
