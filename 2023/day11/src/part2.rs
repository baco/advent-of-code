use core::str::Lines;
use itertools::Itertools;

fn parse_input(lines: Lines<'_>) -> Vec<(usize, usize)> {
    lines
        .clone()
        .enumerate()
        .map(|(i, l)| {
            l.chars()
                .enumerate()
                .filter(|(_, c)| *c == '#')
                .map(|(j, _)| (i, j))
                .collect::<Vec<(usize, usize)>>()
        })
        .fold(Vec::new(), |mut v, mvec| {
            v.extend(mvec);
            v
        })
}

fn expand_galaxies(galaxies: Vec<(usize, usize)>, multiplier: usize) -> Vec<(usize, usize)> {
    let mut tmp_galaxies = galaxies.clone();
    // Expand horizontally
    tmp_galaxies.sort_by(|g1, g2| g1.1.cmp(&g2.1));
    for ((i, g1), (_, g2)) in tmp_galaxies.clone().iter().enumerate().tuple_windows() {
        if g2.1.saturating_sub(1) > g1.1 {
            for galaxy in tmp_galaxies.iter_mut().skip(i + 1) {
                galaxy.1 += (multiplier - 1) * (g2.1 - 1 - g1.1);
            }
        }
    }

    // Expand vertically
    tmp_galaxies.sort_by(|g1, g2| g1.0.cmp(&g2.0));
    for ((i, g1), (_, g2)) in tmp_galaxies.clone().iter().enumerate().tuple_windows() {
        if g2.0.saturating_sub(1) > g1.0 {
            for galaxy in tmp_galaxies.iter_mut().skip(i + 1) {
                galaxy.0 += (multiplier - 1) * (g2.0 - 1 - g1.0);
            }
        }
    }

    tmp_galaxies
}

fn shortest_paths_sum(galaxies: Vec<(usize, usize)>) -> usize {
    let distances_sum = galaxies
        .iter()
        .tuple_combinations()
        .map(|(g1, g2)| (g2.0).abs_diff(g1.0) + (g2.1).abs_diff(g1.1))
        .sum();
    distances_sum
}

pub fn compute(input: &str, multiplier: usize) -> String {
    let galaxies = parse_input(input.lines());
    let expanded_galaxies = expand_galaxies(galaxies, multiplier);
    let shortest_paths_sum = shortest_paths_sum(expanded_galaxies);
    shortest_paths_sum.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
";
    const EXPECTED: usize = 1030; // This expeted value is with a multiplier of 10

    #[test]
    fn it_works() {
        const MULTIPLIER: usize = 10;
        let result = compute(INPUT_S, MULTIPLIER);
        assert_eq!(result, EXPECTED.to_string());
    }
}
