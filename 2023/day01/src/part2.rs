pub fn compute(input: &str) -> String {
    let mut vec = Vec::new();
    for line in input.lines() {
        let mut i: u32 = 0;
        let mut j: u32 = 0;

        for (k, c) in line.chars().enumerate() {
            if c.is_numeric() {
                j = c.to_digit(10).unwrap();
                if i == 0 {
                    i = c.to_digit(10).unwrap();
                }
            } else {
                let n = if line[k..].starts_with("one") {
                    1
                } else if line[k..].starts_with("two") {
                    2
                } else if line[k..].starts_with("three") {
                    3
                } else if line[k..].starts_with("four") {
                    4
                } else if line[k..].starts_with("five") {
                    5
                } else if line[k..].starts_with("six") {
                    6
                } else if line[k..].starts_with("seven") {
                    7
                } else if line[k..].starts_with("eight") {
                    8
                } else if line[k..].starts_with("nine") {
                    9
                } else if line[k..].starts_with("zero") {
                    0
                } else {
                    10
                };
                if n < 10 {
                    if i == 0 {
                        i = n
                    } else {
                        j = n
                    };
                }
            }
        }

        vec.push(i * 10 + j);
    }
    vec.iter().sum::<u32>().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
";
    const EXPECTED: i32 = 281;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
