pub fn compute(input: &str) -> String {
    let mut vec = Vec::new();
    for line in input.lines() {
        let mut i: u32 = 0;
        let mut j: u32 = 0;
        for c in line.chars() {
            if c.is_numeric() {
                j = c.to_digit(10).unwrap();
                if i == 0 {
                    i = c.to_digit(10).unwrap();
                }
            }
        }
        vec.push(i * 10 + j);
    }
    vec.iter().sum::<u32>().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
";
    const EXPECTED: i32 = 142;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
