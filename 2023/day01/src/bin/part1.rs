use day01::part1::compute;

fn main() {
    let file = include_str!("../../input1.txt");
    println!("{}", compute(file));
}
