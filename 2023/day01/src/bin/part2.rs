use day01::part2::compute;

fn main() {
    let file = include_str!("../../input2.txt");
    println!("{}", compute(file));
}
