#[derive(Debug)]
enum Status {
    Operational,
    Damaged,
    Unknown,
}

fn parse_line(line: &str) -> (Vec<Status>, Vec<u8>) {
    let mut line_it = line.split(' ');

    let springs_record = line_it.next().expect("the condition records");
    let springs = springs_record
        .chars()
        .map(|s| match s {
            '.' => Status::Operational,
            '#' => Status::Damaged,
            '?' => Status::Unknown,
            _ => unreachable!(),
        })
        .collect::<Vec<_>>();

    let damaged_groups_record = line_it.last().expect("the damaged groups");
    let damaged_groups = damaged_groups_record
        .split(',')
        .map(|n| n.parse::<u8>().expect("a number"))
        .collect::<Vec<_>>();

    (springs, damaged_groups)
}

pub fn compute(input: &str) -> String {
    let (springs, damaged_groups) = parse_line(input.lines().next().expect("first line"));
    dbg!(&springs, &damaged_groups);
    42.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1
";
    const EXPECTED: usize = 1 + 4 + 1 + 1 + 4 + 10;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
