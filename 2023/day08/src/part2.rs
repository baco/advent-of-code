use crate::gcd_lcm::lcm;
use itertools::Itertools;
use std::collections::HashMap;
use std::str::Lines;

#[derive(Debug)]
enum Cmd {
    L,
    R,
}

fn parse_command_list(line: &str) -> Vec<Cmd> {
    line.chars()
        .map(|c| match c {
            'L' => Cmd::L,
            'R' => Cmd::R,
            _ => unreachable!("not a direction"),
        })
        .collect::<Vec<Cmd>>()
}

fn parse_locations(lines_it: Lines<'_>) -> HashMap<&str, (&str, &str)> {
    lines_it
        .map(|l| {
            l.split(" = ")
                .collect_tuple::<(&str, &str)>()
                .expect("gimme a tuple")
        })
        .map(|(n, d)| (n, (&d[1..4], &d[6..9])))
        .collect::<HashMap<&str, (&str, &str)>>()
}

pub fn compute(input: &str) -> String {
    let mut lines_it = input.lines();
    let mut commands = parse_command_list(lines_it.next().expect("the first line"));
    lines_it.next();

    let map = parse_locations(lines_it);
    let locs = map
        .iter()
        .map(|(&k, &_)| k)
        .filter(|l| l.ends_with('A'))
        .collect::<Vec<_>>();

    // Brute force approach:
    //while !loc.iter().all(|&l| l.ends_with('Z')) {
    //    let direction = commands.remove(0);
    //    for i in 0..loc.len() {
    //        loc[i] = match direction {
    //            Cmd::L => map[loc[i]].0,
    //            Cmd::R => map[loc[i]].1,
    //        }
    //    }
    //    count += 1;
    //    commands.push(direction);
    //}

    // Smarter approach:
    let counts = locs
        .into_iter()
        .map(|mut loc| {
            let mut count = 0usize;
            while !loc.ends_with('Z') {
                let direction = commands.remove(0);
                match direction {
                    Cmd::L => loc = map[loc].0,
                    Cmd::R => loc = map[loc].1,
                }
                count += 1;
                commands.push(direction);
            }
            count
        })
        .collect::<Vec<_>>();
    let ret_lcm = counts
        .into_iter()
        .fold(1, |partial_lcm, count| lcm(partial_lcm, count));

    ret_lcm.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)
";
    const EXPECTED: usize = 6;

    #[test]
    fn trip() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
