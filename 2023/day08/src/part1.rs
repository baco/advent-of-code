use itertools::Itertools;
use std::collections::HashMap;
use std::str::Lines;

#[derive(Debug)]
enum Cmd {
    L,
    R,
}

fn parse_command_list(line: &str) -> Vec<Cmd> {
    line.chars()
        .map(|c| match c {
            'L' => Cmd::L,
            'R' => Cmd::R,
            _ => unreachable!("not a direction"),
        })
        .collect::<Vec<Cmd>>()
}

fn parse_locations(lines_it: Lines<'_>) -> HashMap<&str, (&str, &str)> {
    lines_it
        .map(|l| {
            l.split(" = ")
                .collect_tuple::<(&str, &str)>()
                .expect("gimme a tuple")
        })
        .map(|(n, d)| (n, (&d[1..4], &d[6..9])))
        .collect::<HashMap<&str, (&str, &str)>>()
}

pub fn compute(input: &str) -> String {
    let mut lines_it = input.lines();
    let mut commands = parse_command_list(lines_it.next().expect("the first line"));
    lines_it.next();

    let map = parse_locations(lines_it);
    let mut loc = "AAA";
    let mut count = 0usize;
    while loc != "ZZZ" {
        let direction = commands.remove(0);
        match direction {
            Cmd::L => loc = map[loc].0,
            Cmd::R => loc = map[loc].1,
        }
        count += 1;
        commands.push(direction);
    }

    count.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT1_S: &str = "\
RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
";
    const EXPECTED1: usize = 2;
    static INPUT2_S: &str = "\
LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
";
    const EXPECTED2: usize = 6;

    #[test]
    fn trip_short() {
        let result = compute(INPUT1_S);
        assert_eq!(result, EXPECTED1.to_string());
    }

    #[test]
    fn trip_loop() {
        let result = compute(INPUT2_S);
        assert_eq!(result, EXPECTED2.to_string());
    }
}
