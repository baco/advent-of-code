fn color_power(draw: &str, color: &str) -> u32 {
    let power = draw
        .split("; ")
        .filter(|s| s.contains(color))
        .fold(0, |set_power, set| {
            let color_pwr = set
                .split(", ")
                .find(|d| d.ends_with(color))
                .map(|cubes| {
                    let mut d = cubes.split_whitespace();
                    d.next().expect("").parse::<u32>().expect("")
                })
                .expect("");
            set_power.max(color_pwr)
        });

    power
}

fn compute_line(line: &str) -> u32 {
    let draw = line
        .split(": ")
        .last()
        .expect("should be the draws raw str");
    let line_power = ["red", "green", "blue"]
        .iter()
        .map(|color| color_power(draw, color))
        .product();

    line_power
}

pub fn compute(input: &str) -> String {
    let output = input.lines().map(compute_line).sum::<u32>();
    output.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
";
    const EXPECTED: u32 = 2286;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
