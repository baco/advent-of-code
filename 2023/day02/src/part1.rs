const MAX_RED: u32 = 12;
const MAX_GREEN: u32 = 13;
const MAX_BLUE: u32 = 14;

fn parse_line(line: &str) -> u32 {
    //dbg!(line);
    let mut parts = line.split(": "); //.map(|g| dbg!(g));
    let game_nr = parts
        .next()
        .expect("should be a game raw str")
        .split_whitespace()
        .last()
        .expect("should be a game number str")
        .parse::<u32>()
        .expect("this should be a game number");
    let draws = parts
        .last()
        .expect("should be the draws raw str")
        .split("; ")
        .fold(true, |valid, draw| {
            //dbg!(draw);
            let count = draw.split(", ").fold(true, |valid, cubes| {
                //dbg!(cubes);
                let mut d = cubes.split_whitespace();
                let c = d.next().expect("").parse::<u32>().expect("");
                let color = d.last().expect("this is a color label");
                valid
                    && ((color == "red" && c <= MAX_RED)
                        || (color == "green" && c <= MAX_GREEN)
                        || (color == "blue" && c <= MAX_BLUE))
            });
            //dbg!(&count);
            count && valid
        });
    //let _ = draws.clone().collect::<Vec<_>>();

    if draws {
        game_nr
    } else {
        0
    }
}

pub fn compute(input: &str) -> String {
    let output = input.lines().map(parse_line).sum::<u32>();
    //let _ = output.clone().collect::<Vec<_>>();
    dbg!(output);
    output.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
";
    const EXPECTED: u32 = 8;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
