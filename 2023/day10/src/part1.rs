use core::str::Lines;
use std::collections::HashMap;

type ConnectionsPos = (Option<(usize, usize)>, Option<(usize, usize)>);

#[derive(Debug)]
struct Node {
    tile: char,
    pos: (usize, usize),
    connects: ConnectionsPos,
}

fn get_tile_connections(
    tile: char,
    i: usize,
    j: usize,
    max_i: usize,
    max_j: usize,
) -> ConnectionsPos {
    let (north, south) = match i {
        0 => (None, Some((i + 1, j))),
        i if i == max_i => (Some((i - 1, j)), None),
        _ => (Some((i - 1, j)), Some((i + 1, j))),
    };
    let (east, west) = match j {
        0 => (Some((i, j + 1)), None),
        j if j == max_j => (None, Some((i, j - 1))),
        _ => (Some((i, j + 1)), Some((i, j - 1))),
    };

    match tile {
        '|' => (north, south),
        '-' => (east, west),
        'L' => (north, east),
        'J' => (north, west),
        '7' => (south, west),
        'F' => (south, east),
        'S' => (Some((i, j)), Some((i, j))),
        _ => (None, None),
    }
}

fn parse_map(lines: Lines<'_>) -> HashMap<(usize, usize), Node> {
    let mut map = HashMap::new();
    let max_i = lines.clone().count() - 1;
    let max_j = lines.clone().last().expect("always a line").len() - 1;
    for (i, line) in lines.enumerate() {
        for (j, tile) in line.chars().enumerate() {
            if tile == '.' {
                continue;
            }
            let connects = get_tile_connections(tile, i, j, max_i, max_j);
            map.insert(
                (i, j),
                Node {
                    tile,
                    pos: (i, j),
                    connects,
                },
            );
        }
    }
    map
}

fn walk_maze(
    maze: &HashMap<(usize, usize), Node>,
    source: (usize, usize),
    start: (usize, usize),
) -> usize {
    let mut count = 0usize;
    let mut current_pos = source;
    let mut next_pos = start;
    while next_pos != source {
        let current_node = maze.get(&next_pos).expect("always a node");
        match current_node.connects.0 {
            Some(t) if t != current_pos => next_pos = t,
            Some(_) => match current_node.connects.1 {
                Some(t) if t != current_pos => next_pos = t,
                _ => panic!("aaa"),
            },
            None => panic!("aaa"),
        }
        current_pos = current_node.pos;
        count += 1;
    }
    count += 1;
    count / 2
}

pub fn compute(input: &str) -> String {
    let map = parse_map(input.lines());
    let starting_point = map
        .iter()
        .find_map(|(k, v)| if v.tile == 'S' { Some(k) } else { None })
        .expect("some map possition");

    let possible_starts = map
        .iter()
        .filter_map(|(k, v)| {
            if v.tile != 'S'
                && (v.connects.0 == Some(*starting_point) || v.connects.1 == Some(*starting_point))
            {
                Some((k, v))
            } else {
                None
            }
        })
        .collect::<Vec<_>>();

    let path_length = walk_maze(&map, *starting_point, possible_starts[0].1.pos)
        .max(walk_maze(&map, *starting_point, possible_starts[1].1.pos));

    path_length.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT1_S: &str = "\
.....
.S-7.
.|.|.
.L-J.
.....
";
    const EXPECTED1: usize = 4;

    #[test]
    fn maze1() {
        let result = compute(INPUT1_S);
        assert_eq!(result, EXPECTED1.to_string());
    }

    static INPUT2_S: &str = "\
..F7.
.FJ|.
SJ.L7
|F--J
LJ...
";
    const EXPECTED2: usize = 8;

    #[test]
    fn maze2() {
        let result = compute(INPUT2_S);
        assert_eq!(result, EXPECTED2.to_string());
    }
}
