use core::str::Lines;
use std::collections::HashMap;

type ConnectionsPos = (Option<(usize, usize)>, Option<(usize, usize)>);

#[derive(Debug)]
struct Node {
    tile: char,
    pos: (usize, usize),
    connects: ConnectionsPos,
}

fn get_tile_connections(
    tile: char,
    i: usize,
    j: usize,
    max_i: usize,
    max_j: usize,
) -> ConnectionsPos {
    let (north, south) = match i {
        0 => (None, Some((i + 1, j))),
        i if i == max_i => (Some((i - 1, j)), None),
        _ => (Some((i - 1, j)), Some((i + 1, j))),
    };
    let (east, west) = match j {
        0 => (Some((i, j + 1)), None),
        j if j == max_j => (None, Some((i, j - 1))),
        _ => (Some((i, j + 1)), Some((i, j - 1))),
    };

    match tile {
        '|' => (north, south),
        '-' => (east, west),
        'L' => (north, east),
        'J' => (north, west),
        '7' => (south, west),
        'F' => (south, east),
        'S' => (Some((i, j)), Some((i, j))),
        _ => (None, None),
    }
}

fn parse_map(lines: Lines<'_>) -> HashMap<(usize, usize), Node> {
    let mut map = HashMap::new();
    let max_i = lines.clone().count() - 1;
    let max_j = lines.clone().last().expect("always a line").len() - 1;
    for (i, line) in lines.enumerate() {
        for (j, tile) in line.chars().enumerate() {
            if tile == '.' {
                continue;
            }
            let connects = get_tile_connections(tile, i, j, max_i, max_j);
            map.insert(
                (i, j),
                Node {
                    tile,
                    pos: (i, j),
                    connects,
                },
            );
        }
    }
    map
}

fn walk_maze(
    maze: &HashMap<(usize, usize), Node>,
    source: (usize, usize),
    start: (usize, usize),
) -> Vec<(usize, usize)> {
    let mut positions = Vec::new();
    let mut current_pos = source;
    positions.push(current_pos);
    let mut next_pos = start;
    while next_pos != source {
        let current_node = maze.get(&next_pos).expect("always a node");
        match current_node.connects.0 {
            Some(t) if t != current_pos => next_pos = t,
            Some(_) => match current_node.connects.1 {
                Some(t) if t != current_pos => next_pos = t,
                _ => panic!("aaa"),
            },
            None => panic!("aaa"),
        }
        current_pos = current_node.pos;
        positions.push(current_pos);
    }
    positions
}

pub fn compute(input: &str) -> String {
    let map = parse_map(input.lines());
    let starting_point = map
        .iter()
        .find_map(|(k, v)| if v.tile == 'S' { Some(k) } else { None })
        .expect("some map possition");

    let possible_starts = map
        .iter()
        .filter_map(|(k, v)| {
            if v.tile != 'S'
                && (v.connects.0 == Some(*starting_point) || v.connects.1 == Some(*starting_point))
            {
                Some((k, v))
            } else {
                None
            }
        })
        .collect::<Vec<_>>();

    let path = walk_maze(&map, *starting_point, possible_starts[0].1.pos);

    let mut count = 0;
    for (i, l) in input.lines().enumerate() {
        let mut inside = false;
        for (j, tile) in l.chars().enumerate() {
            if path.contains(&(i, j)) {
                match tile {
                    '|' | 'J' | 'L' => inside = !inside,
                    _ => (),
                }
            } else if inside {
                count += 1;
            }
        }
    }

    count.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT1_S: &str = "\
...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........
";
    static INPUT1_1_S: &str = "\
..........
.S------7.
.|F----7|.
.||....||.
.||....||.
.|L-7F-J|.
.|..||..|.
.L--JL--J.
..........
";
    const EXPECTED1: usize = 4;

    #[test]
    fn maze1() {
        let result = compute(INPUT1_S);
        assert_eq!(result, EXPECTED1.to_string());
    }

    #[test]
    fn maze1_1() {
        let result = compute(INPUT1_1_S);
        assert_eq!(result, EXPECTED1.to_string());
    }

    static INPUT2_S: &str = "\
.F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...
";
    const EXPECTED2: usize = 8;

    #[test]
    fn maze2() {
        let result = compute(INPUT2_S);
        assert_eq!(result, EXPECTED2.to_string());
    }

    static INPUT3_S: &str = "\
FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L
";
    const EXPECTED3: usize = 10;

    #[test]
    fn maze3() {
        let result = compute(INPUT3_S);
        assert_eq!(result, EXPECTED3.to_string());
    }
}
