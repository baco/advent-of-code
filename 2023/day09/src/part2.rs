use core::str::Lines;

fn parse_input(lines: Lines<'_>) -> Vec<Vec<i32>> {
    lines
        .map(|l| {
            l.split_whitespace()
                .map(|n| n.parse::<i32>().unwrap())
                .collect()
        })
        .collect()
}

fn get_prediction(mut history: Vec<i32>) -> i32 {
    let mut sum = history.clone().into_iter().next().expect("an int");
    let mut sign = -1;
    while !history.clone().into_iter().all(|h| h == 0) {
        let mut round = Vec::new();
        for i in 1..history.len() {
            round.push(history[i] - history[i-1]);
        }
        history = round;
        sum += history.clone().into_iter().next().expect("an int") * sign;
        sign *= -1;
    }
    sum
}

pub fn compute(input: &str) -> String {
    let histories = parse_input(input.lines());
    let predictions_sum = histories.into_iter().map(get_prediction).sum::<i32>();
    predictions_sum.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45
";
    const EXPECTED: usize = 2;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
