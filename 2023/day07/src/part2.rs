use std::collections::{HashMap, HashSet};

#[derive(PartialOrd, PartialEq, Ord, Eq, Hash, Clone, Copy, Debug)]
enum Card {
    J,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    T,
    Q,
    K,
    A,
}

#[derive(PartialOrd, PartialEq, Ord, Eq, Debug)]
enum Hand {
    HighCard(Card, Card, Card, Card, Card),
    OnePair(Card, Card, Card, Card, Card),
    TwoPair(Card, Card, Card, Card, Card),
    ThreeKind(Card, Card, Card, Card, Card),
    FullHouse(Card, Card, Card, Card, Card),
    FourKind(Card, Card, Card, Card, Card),
    FiveKind(Card, Card, Card, Card, Card),
}

fn make_hand(hand_vec: &Vec<Card>) -> Hand {
    let mut sorted_hand = hand_vec.clone();
    sorted_hand.sort_by(|a, b| b.cmp(a));

    let top_frequent = hand_vec
        .iter()
        .filter(|c| *c != &Card::J)
        .fold(HashMap::<&Card, usize>::new(), |mut m, x| {
            *m.entry(x).or_default() += 1;
            m
        })
        .into_iter()
        .max_by_key(|(_, v)| *v)
        .map(|(k, _)| k)
        .or(Some(&Card::J))
        .expect("some card");
    let mut sorted_hand = sorted_hand
        .iter()
        .map(|h| match h {
            Card::J => top_frequent,
            _ => h,
        })
        .collect::<Vec<_>>();
    sorted_hand.sort();

    match hand_vec {
        hv if sorted_hand.iter().collect::<HashSet<_>>().len() == 1 => {
            Hand::FiveKind(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if sorted_hand.iter().collect::<HashSet<_>>().len() == 2
            && sorted_hand[1..4].iter().collect::<HashSet<_>>().len() == 1 =>
        {
            Hand::FourKind(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if sorted_hand.iter().collect::<HashSet<_>>().len() == 2 => {
            Hand::FullHouse(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if sorted_hand.iter().collect::<HashSet<_>>().len() == 3
            && (sorted_hand[1..4].iter().collect::<HashSet<_>>().len() == 1
                || sorted_hand[..=2].iter().collect::<HashSet<_>>().len() != 2
                || sorted_hand[2..].iter().collect::<HashSet<_>>().len() != 2) =>
        {
            Hand::ThreeKind(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if sorted_hand.iter().collect::<HashSet<_>>().len() == 3
            && sorted_hand[1..4].iter().collect::<HashSet<_>>().len() != 1
            && sorted_hand[..=2].iter().collect::<HashSet<_>>().len() == 2
            && sorted_hand[2..].iter().collect::<HashSet<_>>().len() == 2 =>
        {
            Hand::TwoPair(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if sorted_hand.iter().collect::<HashSet<_>>().len() == 4 => {
            Hand::OnePair(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if sorted_hand.iter().collect::<HashSet<_>>().len() == 5 => {
            Hand::HighCard(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        _ => unreachable!("Should never happen."),
    }
}

fn parse_input_line(line: &str) -> (Hand, usize) {
    let mut line_vec = line.split_whitespace();
    let hand_str = line_vec.next().expect("a hand str");
    let bid = line_vec
        .last()
        .expect("a bid str")
        .parse::<usize>()
        .expect("the bid");

    let hand_vec = hand_str
        .chars()
        .map(|c| match c {
            'A' => Card::A,
            'K' => Card::K,
            'Q' => Card::Q,
            'J' => Card::J,
            'T' => Card::T,
            '9' => Card::Nine,
            '8' => Card::Eight,
            '7' => Card::Seven,
            '6' => Card::Six,
            '5' => Card::Five,
            '4' => Card::Four,
            '3' => Card::Three,
            '2' => Card::Two,
            _ => unreachable!("Should never happen."),
        })
        .collect::<Vec<_>>();
    let hand = make_hand(&hand_vec);

    (hand, bid)
}

pub fn compute(input: &str) -> String {
    let mut lines_it = input
        .lines()
        .map(parse_input_line)
        .collect::<Vec<(Hand, usize)>>();
    lines_it.sort();

    let total_winnings = lines_it
        .into_iter()
        .enumerate()
        .map(|(i, (_, bid))| (i + 1) * bid)
        .sum::<usize>();

    total_winnings.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
";
    const EXPECTED: usize = 5905;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
