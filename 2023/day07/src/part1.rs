use std::collections::HashSet;

#[derive(PartialOrd, PartialEq, Ord, Eq, Hash, Clone, Copy, Debug)]
enum Card {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    T,
    J,
    Q,
    K,
    A,
}

#[derive(PartialOrd, PartialEq, Ord, Eq, Debug)]
enum Hand {
    HighCard(Card, Card, Card, Card, Card),
    OnePair(Card, Card, Card, Card, Card),
    TwoPair(Card, Card, Card, Card, Card),
    ThreeKind(Card, Card, Card, Card, Card),
    FullHouse(Card, Card, Card, Card, Card),
    FourKind(Card, Card, Card, Card, Card),
    FiveKind(Card, Card, Card, Card, Card),
}

//impl PartialEq for Hand {
//    fn eq(&self, other: &Hand) -> bool {
//        use Hand::*;
//        match (self, other) {
//            (HighCard(c1, c2, c3, c4, c5), HighCard(co1, co2, co3, co4, co5)) => {
//                c1 == co1 && c2 == co2 && c3 == co3 && c4 == co4 && c5 == co5
//            }
//            (FourKind(c1, c2, c3, c4, c5), FourKind(co1, co2, co3, co4, co5)) => {
//                c1 == co1 && c2 == co2 && c3 == co3 && c4 == co4 && c5 == co5
//            }
//            _ => false,
//        }
//    }
//}

fn make_hand(hand_vec: &Vec<Card>) -> Hand {
    let mut sorted_hand = hand_vec.clone();
    sorted_hand.sort();
    match hand_vec {
        hv if hv.iter().collect::<HashSet<_>>().len() == 1 => {
            Hand::FiveKind(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if hv.iter().collect::<HashSet<_>>().len() == 2
            && sorted_hand[1..4].iter().collect::<HashSet<_>>().len() == 1 =>
        {
            Hand::FourKind(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if hv.iter().collect::<HashSet<_>>().len() == 2 => {
            Hand::FullHouse(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if hv.iter().collect::<HashSet<_>>().len() == 3
            && (sorted_hand[1..4].iter().collect::<HashSet<_>>().len() == 1
                || sorted_hand[..=2].iter().collect::<HashSet<_>>().len() != 2
                || sorted_hand[2..].iter().collect::<HashSet<_>>().len() != 2) =>
        {
            Hand::ThreeKind(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if hv.iter().collect::<HashSet<_>>().len() == 3
            && sorted_hand[1..4].iter().collect::<HashSet<_>>().len() != 1
            && sorted_hand[..=2].iter().collect::<HashSet<_>>().len() == 2
            && sorted_hand[2..].iter().collect::<HashSet<_>>().len() == 2 =>
        {
            Hand::TwoPair(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if hv.iter().collect::<HashSet<_>>().len() == 4 => {
            Hand::OnePair(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        hv if hv.iter().collect::<HashSet<_>>().len() == 5 => {
            Hand::HighCard(hv[0], hv[1], hv[2], hv[3], hv[4])
        }
        _ => unreachable!("Should never happen."),
    }
}

fn parse_input_line(line: &str) -> (Hand, usize) {
    let mut line_vec = line.split_whitespace();
    let hand_str = line_vec.next().expect("a hand str");
    let bid = line_vec
        .last()
        .expect("a bid str")
        .parse::<usize>()
        .expect("the bid");

    let hand_vec = hand_str
        .chars()
        .map(|c| match c {
            'A' => Card::A,
            'K' => Card::K,
            'Q' => Card::Q,
            'J' => Card::J,
            'T' => Card::T,
            '9' => Card::Nine,
            '8' => Card::Eight,
            '7' => Card::Seven,
            '6' => Card::Six,
            '5' => Card::Five,
            '4' => Card::Four,
            '3' => Card::Three,
            '2' => Card::Two,
            _ => unreachable!("Should never happen."),
        })
        .collect::<Vec<_>>();
    let hand = make_hand(&hand_vec);

    (hand, bid)
}

pub fn compute(input: &str) -> String {
    let mut lines_it = input
        .lines()
        .map(parse_input_line)
        .collect::<Vec<(Hand, usize)>>();
    lines_it.sort();

    let total_winnings = lines_it
        .into_iter()
        .enumerate()
        .fold(0, |s, (i, (_, bid))| s + (i + 1) * bid);

    total_winnings.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
";
    const EXPECTED: usize = 6440;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
