use day07::part2::compute;

fn main() {
    let file = include_str!("../../input.txt");
    println!("{}", compute(file));
}
