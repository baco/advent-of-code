fn parse_cards(input: &str) -> Vec<(Vec<u32>, Vec<u32>)> {
    let cards: Vec<(Vec<u32>, Vec<u32>)> = input
        .lines()
        .map(|line| {
            let mut card = line.split(": ").last().expect("rhs").split(" | ");
            let winning_nrs: Vec<u32> = card
                .next()
                .expect("nrs")
                .split_whitespace()
                .map(|n| n.parse::<u32>().unwrap())
                .collect();
            let have_nrs: Vec<u32> = card
                .next()
                .expect("nrs")
                .split_whitespace()
                .map(|n| n.parse::<u32>().unwrap())
                .collect();
            (winning_nrs, have_nrs)
        })
        .collect();
    cards
}

pub fn compute(input: &str) -> String {
    let cards = parse_cards(input);
    let wins_streak: Vec<u32> = cards
        .into_iter()
        .map(|(wins, have)| {
            let actual_wins = wins
                .iter()
                .filter(|n| have.contains(n))
                .count()
                .try_into()
                .unwrap();
            actual_wins
        })
        .collect();
    dbg!(&wins_streak);
    let mut card_ammounts = vec![1_u32; wins_streak.len()];
    for (i, points) in wins_streak.into_iter().enumerate() {
        for j in i + 1..i + 1 + (points as usize) {
            card_ammounts[j] += card_ammounts[i];
        }
        dbg!(&card_ammounts);
    }
    let cards_count: u32 = card_ammounts.into_iter().sum();
    cards_count.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
";
    const EXPECTED: u32 = 30;

    #[test]
    fn it_works() {
        // The simulated representation of values would be:
        // 4
        // 22
        // 2222
        // 11111111
        // 00  000000000000
        // 0
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
