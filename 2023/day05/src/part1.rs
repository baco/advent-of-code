use itertools::Itertools;
use std::str::Lines;

fn parse_needed_seeds(line: &str) -> Vec<usize> {
    line.split(": ")
        .last()
        .expect("a list of int")
        .split(' ')
        .map(|n| n.parse::<usize>().expect("some number"))
        .collect::<Vec<usize>>()
}

#[derive(Debug)]
struct Almanac {
    seed_to_soil: Vec<Transform>,
    soil_to_fertilizer: Vec<Transform>,
    fertilizer_to_water: Vec<Transform>,
    water_to_light: Vec<Transform>,
    light_to_temperature: Vec<Transform>,
    temperature_to_humidity: Vec<Transform>,
    humidity_to_location: Vec<Transform>,
}

#[derive(Debug)]
struct Transform {
    from: usize,
    to: usize,
    range: usize,
}

impl Transform {
    fn convert(&self, n: &usize) -> usize {
        if !(self.from..self.from + self.range).contains(n) {
            return *n;
        }
        self.to + (*n - self.from)
    }
}

fn parse_almanac(mut lines: Lines<'_>) -> Almanac {
    let mut almanac = Almanac {
        seed_to_soil: Vec::new(),
        soil_to_fertilizer: Vec::new(),
        fertilizer_to_water: Vec::new(),
        water_to_light: Vec::new(),
        light_to_temperature: Vec::new(),
        temperature_to_humidity: Vec::new(),
        humidity_to_location: Vec::new(),
    };
    lines.next();
    lines.next();

    while let Some(line) = lines.next() {
        if line.is_empty() {
            lines.next();
            break;
        }
        let (to, from, range) = line
            .split(' ')
            .map(|n| n.parse::<usize>().expect("numbers"))
            .collect_tuple::<(usize, usize, usize)>()
            .expect("gimme a tuple");
        almanac.seed_to_soil.push(Transform { from, to, range });
    }

    while let Some(line) = lines.next() {
        if line.is_empty() {
            lines.next();
            break;
        }
        let (to, from, range) = line
            .split(' ')
            .map(|n| n.parse::<usize>().expect("numbers"))
            .collect_tuple::<(usize, usize, usize)>()
            .expect("gimme a tuple");
        almanac
            .soil_to_fertilizer
            .push(Transform { from, to, range });
    }

    while let Some(line) = lines.next() {
        if line.is_empty() {
            lines.next();
            break;
        }
        let (to, from, range) = line
            .split(' ')
            .map(|n| n.parse::<usize>().expect("numbers"))
            .collect_tuple::<(usize, usize, usize)>()
            .expect("gimme a tuple");
        almanac
            .fertilizer_to_water
            .push(Transform { from, to, range });
    }

    while let Some(line) = lines.next() {
        if line.is_empty() {
            lines.next();
            break;
        }
        let (to, from, range) = line
            .split(' ')
            .map(|n| n.parse::<usize>().expect("numbers"))
            .collect_tuple::<(usize, usize, usize)>()
            .expect("gimme a tuple");
        almanac.water_to_light.push(Transform { from, to, range });
    }

    while let Some(line) = lines.next() {
        if line.is_empty() {
            lines.next();
            break;
        }
        let (to, from, range) = line
            .split(' ')
            .map(|n| n.parse::<usize>().expect("numbers"))
            .collect_tuple::<(usize, usize, usize)>()
            .expect("gimme a tuple");
        almanac
            .light_to_temperature
            .push(Transform { from, to, range });
    }

    while let Some(line) = lines.next() {
        if line.is_empty() {
            lines.next();
            break;
        }
        let (to, from, range) = line
            .split(' ')
            .map(|n| n.parse::<usize>().expect("numbers"))
            .collect_tuple::<(usize, usize, usize)>()
            .expect("gimme a tuple");
        almanac
            .temperature_to_humidity
            .push(Transform { from, to, range });
    }

    while let Some(line) = lines.next() {
        if line.is_empty() {
            lines.next();
            break;
        }
        let (to, from, range) = line
            .split(' ')
            .map(|n| n.parse::<usize>().expect("numbers"))
            .collect_tuple::<(usize, usize, usize)>()
            .expect("gimme a tuple");
        almanac
            .humidity_to_location
            .push(Transform { from, to, range });
    }
    almanac
}

fn get_next_batch(current_batch: Vec<usize>, transforms: &Vec<Transform>) -> Vec<usize> {
    let mut next_batch = Vec::new();
    for seed in &current_batch {
        for (i, tr) in transforms.iter().enumerate() {
            let new_seed = tr.convert(seed);
            if new_seed != *seed {
                next_batch.push(new_seed);
                break;
            }
            if i == transforms.len() - 1 {
                next_batch.push(new_seed);
            }
        }
    }
    next_batch
}

pub fn compute(input: &str) -> String {
    let mut lines_it = input.lines();
    let seeds_to_plant = parse_needed_seeds(lines_it.next().expect("the first line"));
    let almanac = parse_almanac(lines_it);

    let next_batch = get_next_batch(seeds_to_plant.clone(), &almanac.seed_to_soil);
    let next_batch = get_next_batch(next_batch.clone(), &almanac.soil_to_fertilizer);
    let next_batch = get_next_batch(next_batch.clone(), &almanac.fertilizer_to_water);
    let next_batch = get_next_batch(next_batch.clone(), &almanac.water_to_light);
    let next_batch = get_next_batch(next_batch.clone(), &almanac.light_to_temperature);
    let next_batch = get_next_batch(next_batch.clone(), &almanac.temperature_to_humidity);
    let location = get_next_batch(next_batch.clone(), &almanac.humidity_to_location);

    let lowest = location.into_iter().min().expect("a number");
    lowest.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
";
    const EXPECTED: usize = 35;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
