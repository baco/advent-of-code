use core::str::Lines;

#[derive(Debug)]
struct Record {
    time: usize,
    distance: usize,
}

fn parse_input(mut lines: Lines<'_>) -> Vec<Record> {
    let times = lines
        .next()
        .expect("times line")
        .split(':')
        .last()
        .expect("the times")
        .split_whitespace()
        .map(|t| t.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    let distances = lines
        .next()
        .expect("distances line")
        .split(':')
        .last()
        .expect("the distances")
        .split_whitespace()
        .map(|d| d.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    let mut records = Vec::new();
    for (time, distance) in times.iter().zip(distances.iter()) {
        records.push(Record {
            time: *time,
            distance: *distance,
        });
    }
    records
}

pub fn compute(input: &str) -> String {
    let records = parse_input(input.lines());
    dbg!(&records);
    let total_possibilities = records
        .iter()
        .map(|r| {
            let passing = (0..=r.time)
                .filter_map(|t| (t * (r.time - t) > r.distance).then_some(t * (r.time - t)))
                .count();
            dbg!(&passing);
            passing
        })
        .product::<usize>();

    total_possibilities.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
Time:      7  15   30
Distance:  9  40  200
";
    const EXPECTED: usize = 288;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
