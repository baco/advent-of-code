use core::str::Lines;

#[derive(Debug)]
struct Record {
    time: usize,
    distance: usize,
}

fn parse_input(mut lines: Lines<'_>) -> Record {
    let time = lines
        .next()
        .expect("times line")
        .split(':')
        .last()
        .expect("the time")
        .replace(' ', "")
        .parse::<usize>()
        .unwrap();
    let distance = lines
        .next()
        .expect("distances line")
        .split(':')
        .last()
        .expect("the distance")
        .replace(' ', "")
        .parse::<usize>()
        .unwrap();
    Record { time, distance }
}

pub fn compute(input: &str) -> String {
    let record = parse_input(input.lines());
    let passing = (0..=record.time)
        .filter_map(|t| (t * (record.time - t) > record.distance).then_some(t * (record.time - t)))
        .count();
    passing.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
Time:      7  15   30
Distance:  9  40  200
";
    const EXPECTED: usize = 71503;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
