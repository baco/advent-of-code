fn get_number(line: &str) -> u32 {
    let mut char_iter = line.chars();
    let mut n = 0;
    while let Some(d) = char_iter.next().expect("not a char").to_digit(10) {
        n = n * 10 + d
    }
    n
}

fn find_backwards_start(schem: &Vec<Vec<char>>, i: &usize, j: &usize) -> usize {
    let mut start = *j;
    while start > 0 && schem[*i][start - 1].is_ascii_digit() {
        start -= 1;
    }
    start
}

pub fn compute(input: &str) -> String {
    let mut schems: Vec<Vec<char>> = Vec::new();
    for line in input.lines() {
        schems.push(line.chars().collect());
    }

    let mut n = 0;
    for i in 0..schems.len() {
        for j in 0..schems[i].len() {
            let mut north = 1;
            let mut ne = 1;
            let mut east = 1;
            let mut se = 1;
            let mut south = 1;
            let mut sw = 1;
            let mut west = 1;
            let mut nw = 1;
            if schems[i][j] != '*' {
                continue;
            }
            println!("Found Gear @{i}:{j}");

            let special_at_top = i > 0 && schems[i - 1][j].is_ascii_digit();
            let special_at_bottom = i < schems.len() - 1 && schems[i + 1][j].is_ascii_digit();

            let mut adjacents = 0;
            // Case gear in the middle
            if i > 0 && i < schems.len() - 1 && j > 0 && j < schems[i].len() - 1 {
                if !special_at_top {
                    if schems[i - 1][j - 1].is_ascii_digit() {
                        adjacents += 1;
                        let start = find_backwards_start(&schems, &(i - 1), &(j - 1));
                        nw = get_number(&input.lines().nth(i - 1).expect("")[start..]);
                    }
                    if schems[i - 1][j + 1].is_ascii_digit() {
                        adjacents += 1;
                        let start = find_backwards_start(&schems, &(i - 1), &(j + 1));
                        ne = get_number(&input.lines().nth(i - 1).expect("")[start..]);
                    }
                } else {
                    adjacents += 1;
                    let start = find_backwards_start(&schems, &(i - 1), &(j));
                    north = get_number(&input.lines().nth(i - 1).expect("")[start..]);
                }
                if !special_at_bottom {
                    if schems[i + 1][j - 1].is_ascii_digit() {
                        adjacents += 1;
                        let start = find_backwards_start(&schems, &(i + 1), &(j - 1));
                        sw = get_number(&input.lines().nth(i + 1).expect("")[start..]);
                    }
                    if schems[i + 1][j + 1].is_ascii_digit() {
                        adjacents += 1;
                        let start = find_backwards_start(&schems, &(i + 1), &(j + 1));
                        se = get_number(&input.lines().nth(i + 1).expect("")[start..]);
                    }
                } else {
                    adjacents += 1;
                    let start = find_backwards_start(&schems, &(i + 1), &(j));
                    south = get_number(&input.lines().nth(i + 1).expect("")[start..]);
                }
                if schems[i][j - 1].is_ascii_digit() {
                    adjacents += 1;
                    let start = find_backwards_start(&schems, &i, &(j - 1));
                    west = get_number(&input.lines().nth(i).expect("")[start..]);
                }
                if schems[i][j + 1].is_ascii_digit() {
                    adjacents += 1;
                    let start = find_backwards_start(&schems, &i, &(j + 1));
                    east = get_number(&input.lines().nth(i).expect("")[start..]);
                }
            }
            if adjacents == 2 {
                dbg!([north, ne, east, se, south, sw, west, nw]);
                n += [north, ne, east, se, south, sw, west, nw]
                    .iter()
                    .product::<u32>();
                dbg!(n);
            }
        }
    }
    n.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
";
    const EXPECTED: u32 = 467835;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
