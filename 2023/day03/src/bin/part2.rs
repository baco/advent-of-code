use day03::part2::compute;

fn main() {
    let file = include_str!("../../input1.txt");
    println!("{}", compute(file));
}
