fn get_number(line: &str) -> u32 {
    let mut char_iter = line.chars();
    let mut n = 0;
    while let Some(d) = char_iter.next().expect("not a char").to_digit(10) {
        n = n * 10 + d
    }
    n
}

pub fn compute(input: &str) -> String {
    let mut schems: Vec<Vec<char>> = Vec::new();
    for line in input.lines() {
        schems.push(line.chars().collect());
    }
    let mut n = 0;
    dbg!(schems[0][5].is_digit(10));
    for i in 0..schems.len() {
        let mut tmp_n = 0;
        let mut marked = false;
        for j in 0..schems[i].len() {
            if !schems[i][j].is_ascii_digit() {
                marked = false;
                continue;
            }
            if j == 0 || (j > 0 && !schems[i][j - 1].is_ascii_digit()) {
                dbg!(&input.lines().nth(i).expect("")[j..]);
                tmp_n = get_number(&input.lines().nth(i).expect("")[j..]);
            }
            dbg!(i, j, tmp_n);

            if i > 0 && j > 0 && i < schems.len() - 1 && !marked {
                if (schems[i][j - 1] != '.' && !schems[i][j - 1].is_ascii_digit())
                    || (schems[i - 1][j - 1] != '.' && !schems[i - 1][j - 1].is_ascii_digit())
                    || (schems[i - 1][j] != '.' && !schems[i - 1][j].is_ascii_digit())
                    || (schems[i + 1][j - 1] != '.' && !schems[i + 1][j - 1].is_ascii_digit())
                    || (schems[i + 1][j] != '.' && !schems[i + 1][j].is_ascii_digit())
                {
                    n += tmp_n;
                    marked = true;
                }
            } else if i == 0 && j > 0 && j < schems[i].len() - 1 && !marked {
                if (schems[i][j - 1] != '.' && !schems[i][j - 1].is_ascii_digit())
                    || (schems[i + 1][j - 1] != '.' && !schems[i + 1][j - 1].is_ascii_digit())
                    || (schems[i + 1][j] != '.' && !schems[i + 1][j].is_ascii_digit())
                {
                    n += tmp_n;
                    marked = true;
                }
            } else if j == 0 && i > 0 && i < schems.len() - 1 && !marked {
                if (schems[i - 1][j] != '.' && !schems[i - 1][j].is_ascii_digit())
                    || (schems[i + 1][j] != '.' && !schems[i + 1][j].is_ascii_digit())
                {
                    n += tmp_n;
                    marked = true;
                }
            } else if i == schems.len() - 1 && j > 0 && j < schems[i].len() - 1 && !marked {
                if (schems[i][j - 1] != '.' && !schems[i][j - 1].is_ascii_digit())
                    || (schems[i - 1][j - 1] != '.' && !schems[i - 1][j - 1].is_ascii_digit())
                    || (schems[i - 1][j] != '.' && !schems[i - 1][j].is_ascii_digit())
                {
                    n += tmp_n;
                    marked = true;
                }
            } else if i == 0 && j == 0 && !marked {
                if schems[i + 1][j] != '.' && !schems[i + 1][j].is_ascii_digit() {
                    n += tmp_n;
                    marked = true;
                }
            } else if i == schems.len() - 1 && j == 0 && !marked {
                if schems[i - 1][j] != '.' && !schems[i - 1][j].is_ascii_digit() {
                    n += tmp_n;
                    marked = true;
                }
            }
            if j < schems[i].len() - 2 && !schems[i][j + 1].is_ascii_digit() && !marked {
                if schems[i][j + 1] != '.'
                    || (i != 0 && schems[i - 1][j + 1] != '.')
                    || (i != schems.len() && schems[i + 1][j + 1] != '.')
                {
                    n += tmp_n;
                    marked = true;
                }
            }
        }
    }

    n.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT_S: &str = "\
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
";
    const EXPECTED: u32 = 4361;

    #[test]
    fn it_works() {
        let result = compute(INPUT_S);
        assert_eq!(result, EXPECTED.to_string());
    }
}
